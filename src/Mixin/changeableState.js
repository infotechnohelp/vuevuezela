var changeableStateMixin = {
    data: function () {
        return {
            stateChanged: false,
            defaultStateContents: null,
            defaultStateCss: {}
        }
    },
    props: {
        state_changed_config: {
            type: Object
        }
    },
    watch: {
        stateChanged: function (value) {
            var stateChangedConfig = this.$props.state_changed_config;
            var $this = this;

            if (value) {

                if (typeof stateChangedConfig === 'undefined') {
                    return null;
                }

                if (typeof stateChangedConfig.change_contents !== 'undefined') {
                    jQuery(this.$el).text(stateChangedConfig.change_contents);
                }

                if (typeof stateChangedConfig.add_class !== 'undefined') {
                    if (typeof stateChangedConfig.add_class === 'string') {
                        jQuery(this.$el).addClass(stateChangedConfig.add_class);
                    } else {
                        jQuery.each(stateChangedConfig.add_class, function (index, classTitle) {
                            jQuery($this.$el).addClass(classTitle);
                        });
                    }
                }

                if (typeof stateChangedConfig.add_css !== 'undefined') {
                    jQuery.each(stateChangedConfig.add_css, function (key, value) {
                        if(typeof $this.$data.defaultStateCss[key] === 'undefined'){
                            $this.$data.defaultStateCss[key] = jQuery($this.$el).css(key);
                        }

                        jQuery($this.$el).css(key, value);
                    });
                }

            } else {

                if (typeof stateChangedConfig === 'undefined') {
                    return null;
                }

                if (typeof stateChangedConfig.change_contents !== 'undefined') {
                    jQuery(this.$el).text(this.$data.defaultStateContents);
                }

                if (typeof stateChangedConfig.add_class !== 'undefined') {
                    if (typeof stateChangedConfig.add_class === 'string') {
                        if (jQuery(this.$el).hasClass(stateChangedConfig.add_class)) {
                            jQuery($this.$el).removeClass(stateChangedConfig.add_class);
                        }
                    } else {
                        jQuery.each(stateChangedConfig.add_class, function (index, classTitle) {
                            if (jQuery($this.$el).hasClass(classTitle)) { // @todo Looks to be obsolete
                                jQuery($this.$el).removeClass(classTitle);
                            }
                        });
                    }
                }

                if (typeof stateChangedConfig.add_css !== 'undefined') {
                    jQuery.each(stateChangedConfig.add_css, function (key, value) {
                        jQuery($this.$el).css(key, $this.$data.defaultStateCss[key]);
                    });
                }
            }
        }
    },
    mounted: function () {
        this.$data.defaultStateContents = jQuery(this.$el).text().trim();
    }
};