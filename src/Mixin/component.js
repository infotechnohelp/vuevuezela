var componentMixin = {
    data: function () {
        return {
            aliasTag: null
        }
    },
    computed: {
        tag: function () {
            if (this.$data.aliasTag !== null) {
                return this.$data.aliasTag;
            }

            return this.$vnode.componentOptions.tag;
        },
        ref: function () {
            return this.$vnode.data.ref;
        }
    }
};