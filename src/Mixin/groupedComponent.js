var groupedComponentMixin = {
    data: function () {
        return {
            groupMemberTagPrefix: null
        }
    },
    computed: {
        isGroupMember: function () {
            return (typeof this.$parent.tag !== 'undefined') && this.$parent.tag.slice(-5) === 'group';
        },
        groupedComponentSiblings: function () {
            if (this.isGroupMember) {
                return this.$parent.groupMembers;
            } else {
                return new Root(this)
                    .findByTagPrefix(this.$data.groupMemberTagPrefix)
                    .scope;
            }
        },
        groupedComponentTwins: function () {
            if (this.isGroupMember) {
                return new Root(this)
                    .findByTag(this.$parent.tag)
                    .filterByRef(this.$parent.ref)
                    .findByTagPrefix(this.$data.groupMemberTagPrefix)
                    .filterByRef(this.ref)
                    .scope;
            } else {
                return new Root(this)
                    .findByTagPrefix(this.$data.groupMemberTagPrefix)
                    .filterByRef(this.ref)
                    .scope;
            }
        },
        groupedComponentSuperTwins: function () {
            if (this.isGroupMember) {
                return new Root(this)
                    .findByTag(this.$parent.tag)
                    .filterByRef(this.$parent.ref)
                    .findByTag(this.tag)
                    .filterByRef(this.ref)
                    .scope;
            } else {
                return new Root(this)
                    .findByTag(this.tag)
                    .filterByRef(this.ref)
                    .scope;
            }
        }
    }
};