var componentGroupMixin = {
    computed: {
        groupMemberTagPrefix: function () {
            return this.tag.split('-group')[0];
        },
        groupSiblings: function () {
            return new Root(this)
                .findByTag(this.tag)
                .filterByRef(this.ref)
                .scope;
        },
        groupMembers: function () {
            return new Root(this)
                .findByTag(this.tag)
                .filterByRef(this.ref)
                .findByTagPrefix(this.groupMemberTagPrefix)
                .scope;
        }
    }
};