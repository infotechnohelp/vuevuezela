var rootMixin = {
    methods: {
        groupMatched: function (groupComponent, config) {

            var result = true;

            var configStatements = this.prepareConfigStatements(config);

            if (groupComponent.tag !== config.groupTag) {
                result = false;
            }

            if (configStatements.groupHasRef && groupComponent.ref !== config.groupRef) {
                result = false;
            }

            return result;
        },
        componentMatched: function (component, config) {

            var result = true;

            var configStatements = this.prepareConfigStatements(config);

            if (configStatements.findByComponentTagPrefix) {
                if (component.tag.indexOf(config.componentTagPrefix) === -1 || component.tag.slice(-5) === 'group') {
                    result = false;
                }
            } else {
                if (component.tag !== config.componentTag) {
                    result = false;
                }
            }

            if (configStatements.componentHasRef && component.ref !== config.componentRef) {
                result = false;
            }

            return result;
        },
        //groupRef, groupTag, componentTagPrefix, componentTag, componentRef
        prepareConfigStatements: function (config) {

            var isGroupMember = typeof config.groupTag !== 'undefined';

            if (isGroupMember) {
                var groupHasRef = typeof config.groupRef !== 'undefined';
            }

            var findByComponentTagPrefix = typeof config.componentTagPrefix !== 'undefined';

            if (findByComponentTagPrefix) {
                if (typeof config.componentTag !== 'undefined') {
                    throw new Error("Either 'componentTagPrefix' or 'componentTag' may be provided in config for " +
                        "$root.findFirst(), both provided");
                }
            } else {
                if (typeof config.componentTag === 'undefined') {
                    throw new Error("Either 'componentTagPrefix' or 'componentTag' should be provided in config for " +
                        "$root.findFirst()");
                }
            }

            var componentHasRef = typeof config.componentRef !== 'undefined';

            return {
                isGroupMember: isGroupMember,
                groupHasRef: groupHasRef,
                findByComponentTagPrefix: findByComponentTagPrefix,
                componentHasRef: componentHasRef
            };
        },
        findFirst: function (config) {

            var $this = this;

            var configStatements = this.prepareConfigStatements(config);

            var result = null;

            if (configStatements.isGroupMember) {

                jQuery.each(this.$root.$children, function (index, component) {

                    if ($this.groupMatched(component, config)) {

                        jQuery.each(component.$children, function (index, groupMember) {

                            if ($this.componentMatched(groupMember, config)) {
                                result = groupMember;
                                return false;
                            }

                        });

                    }

                    if (result !== null) {
                        return false;
                    }
                });

            } else {

                jQuery.each(this.$root.$children, function (index, component) {
                    if ($this.componentMatched(component, config)) {
                        result = component;
                        return false;
                    }
                });

            }

            return result;
        },
        findAll: function (config) {

            var $this = this;

            var configStatements = this.prepareConfigStatements(config);

            var result = [];

            if (configStatements.isGroupMember) {

                jQuery.each(this.$root.$children, function (index, component) {

                    if ($this.groupMatched(component, config)) {

                        jQuery.each(component.$children, function (index, groupMember) {

                            if ($this.componentMatched(groupMember, config)) {
                                result.push(groupMember);
                            }

                        });

                    }
                });

            } else {

                jQuery.each(this.$root.$children, function (index, component) {
                    if ($this.componentMatched(component, config)) {
                        result.push(component);
                    }
                });

            }

            return result;
        }
    }
};