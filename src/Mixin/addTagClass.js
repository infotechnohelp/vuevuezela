var addTagClassMixin = {
    mounted: function () {
        jQuery(this.$el).addClass(this.$vnode.componentOptions.tag);
    }
};