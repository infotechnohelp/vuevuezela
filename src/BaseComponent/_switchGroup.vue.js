var _switchGroupComponent = Vue.component('_switch-group', {
    mixins: [componentMixin, componentGroupMixin, _basicDivSlotTemplateMixin],
    props: {
        multiple_active_members: {
            type: Boolean,
            default: false
        }
    },
    computed: {
        hasMultipleActiveMembers: function () {
            var result = false;

            jQuery.each(this.groupSiblings, function (index, switchGroup) {
                if (switchGroup.$props.multiple_active_members) {
                    result = true;
                }
            });

            return result;
        }
    }
});