var _switchTargetCloseComponent = Vue.component('_switch-target-close', {
    mixins: [componentMixin],
    computed: {
        switchTargetCloseTargetSwitchTagPrefix: function () {
            var targetSwitchPrefix = this.$vnode.componentOptions.tag.split('-close')[0];

            return targetSwitchPrefix + '-switch';
        }
    },
    methods: {
        clicked: function () {
            jQuery.each(this.$parent.firstSwitch.groupedComponentTwins, function (index, switchTwin) {
                switchTwin.$data.switchActivated = false;
            });
        }
    }
});