var _tabSwitchComponent = Vue.component('_tab-switch', {
    extends: _switchComponent,
    data: function () {
        return {
            // groupedComponentMixin
            groupMemberTagPrefix: 'tab-switch'
        }
    }
});