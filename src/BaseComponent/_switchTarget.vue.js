var _switchTargetComponent = Vue.component('_switch-target', {
    mixins: [componentMixin, groupedComponentMixin, _basicDivSlotTemplateMixin],
    data: function () {
        return {
            show: false,
            shownStateCss: {
                display: null
            }
        };
    },
    computed: {
        firstSwitch: function () {
            var switchRef = this.ref;

            var switchGroupTag = this.tag;

            if (switchGroupTag.slice(-9) === 'component') {
                switchGroupTag = switchGroupTag.split('-component')[0];
            }
            var switchTagPrefix = switchGroupTag + '-switch';

            switchGroupTag += '-switch-group';


            if (this.isGroupMember) {
                var switchGroupRef = this.$parent.ref;

                return new Root(this)
                    .findByTag(switchGroupTag)
                    .filterByRef(switchGroupRef)
                    .findByTagPrefix(switchTagPrefix)
                    .filterByRef(switchRef)
                    .first();

            } else {
                return new Root(this)
                    .findByTagPrefix(switchTagPrefix)
                    .filterByRef(switchRef)
                    .first();
            }
        }
    },
    watch: {
        show: function (value) {
            if (value) {
                jQuery(this.$el).css('display', this.$data.shownStateCss.display);
            } else {
                jQuery(this.$el).css('display', 'none');
            }
        }
    },
    mounted: function () {
        this.$data.shownStateCss.display = jQuery(this.$el).css('display');
        jQuery(this.$el).css('display', 'none');
    }
});