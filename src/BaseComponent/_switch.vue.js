var _switchComponent = Vue.component('_switch-component', {
    mixins: [componentMixin, groupedComponentMixin, changeableStateMixin],
    data: function () {
        return {
            switchActivated: false
        }
    },
    props: {
        refresh_on_click: {
            type: Boolean,
            default: true
        }
    },
    computed: {
        firstSwitchTarget: function () {

            var targetComponentTagPrefix = this.$data.groupMemberTagPrefix.split('-switch')[0];
            var result = null;

            if (this.isGroupMember) {
                var targetGroupRef = this.$parent.ref;
                var targetGroupTag = this.$data.groupMemberTagPrefix.split('switch')[0] + 'group';

                return new Root(this)
                    .findByTag(targetGroupTag)
                    .filterByRef(targetGroupRef)
                    .findByTagPrefix(targetComponentTagPrefix)
                    .filterByRef(this.ref)
                    .first();

            } else {

                new Root(this)
                    .findByTagPrefix(targetComponentTagPrefix)
                    .filterByRef(this.ref)
                    .each(function (index, component) {
                        if (component.tag.indexOf('switch') === -1) {
                            result = component;
                        }
                    });
            }

            return result;
        },
        refreshOnClick: function () {
            var result = true;

            jQuery.each(this.groupedComponentTwins, function (index, switchTwin) {
                if (typeof switchTwin.$props.refresh_on_click !== 'undefined' && !switchTwin.$props.refresh_on_click) {
                    result = false;
                }
            });

            return result;
        }
    },
    watch: {
        switchActivated: function (value) {

            if (value) {
                jQuery.each(this.firstSwitchTarget.groupedComponentTwins, function (index, component) {
                    component.$data.show = true;
                });

                this.$data.stateChanged = true;

            } else {
                jQuery.each(this.firstSwitchTarget.groupedComponentTwins, function (index, component) {
                    component.$data.show = false;
                });

                this.$data.stateChanged = false;
            }
        },
        stateChanged: function (value) {
            if (value) {
                jQuery(this.$el).addClass('active');
            } else {
                jQuery(this.$el).removeClass('active');
            }
        }
    },
    methods: {
        clicked: function () {

            var $this = this;
            var activate = !this.$data.switchActivated;
            var hasSingleActiveSibling = (this.isGroupMember && !this.$parent.hasMultipleActiveMembers);

            if (hasSingleActiveSibling) {

                if (activate) {
                    jQuery.each(this.groupedComponentSiblings, function (index, switchSibling) {
                        switchSibling.$data.switchActivated = (switchSibling.ref === $this.ref);
                    });
                } else {
                    jQuery.each(this.groupedComponentSiblings, function (index, switchSibling) {
                        if (switchSibling.ref === $this.ref) {
                            switchSibling.$data.switchActivated = false;
                        }
                    });
                }

            } else {

                if (activate) {
                    jQuery.each(this.groupedComponentTwins, function (index, switchTwin) {
                        switchTwin.$data.switchActivated = true;
                    });
                } else {
                    if (this.refreshOnClick) {
                        jQuery.each(this.groupedComponentTwins, function (index, switchTwin) {
                            switchTwin.$data.switchActivated = false;
                        });
                    }
                }

            }

        }
    }
});