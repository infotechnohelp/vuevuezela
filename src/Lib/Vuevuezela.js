var Vuevuezela = function () {
    this.eventListeners = {};
};

Vuevuezela.prototype.getTemplateInjections = function (instruction) {

    var vuevuezela = this;
    var result = [];

    jQuery.each(instruction.functionalities, function (index, functionality) {
        if (functionality.templateInjection !== undefined) {
            result.push(functionality.templateInjection);
        }
    });

    if(instruction.extending !== undefined){
        result = result.concat(vuevuezela.getTemplateInjections(instruction.extending.instruction));
    }

    return result;
};

Vuevuezela.prototype.prepareTemplate = function (template, templateInjections) {

    var templateInjectionString = '';

    jQuery.each(templateInjections, function (index, templateInjection) {
        templateInjectionString += ' ' + templateInjection;
    });

    var position = template.indexOf(">");

    return [template.slice(0, position), templateInjectionString, template.slice(position)].join('');
};

var vuevuezela = new Vuevuezela();