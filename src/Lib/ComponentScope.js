var ComponentScope = function (componentArray) {
    this.scope = componentArray;
};

ComponentScope.prototype.findAll = function () {

    var result = [];

    jQuery.each(this.scope, function (index, component) {
        jQuery.each(component.$children, function (index, child) {
            result.push(child);
        });
    });

    if (result.length === 0) {
        console.warn(this.scope);
        throw new Error('Cannot find any component in component scope');
    }

    this.scope = result;

    return this;
};

ComponentScope.prototype.findByRef = function (ref) {

    var result = [];

    jQuery.each(this.scope, function (index, component) {
        jQuery.each(component.$children, function (index, child) {
            if (child.ref === ref) {
                result.push(child);
            }
        });
    });

    if (result.length === 0) {
        console.warn(this.scope);
        throw new Error('Cannot find any component by ref "' + ref + '" in component scope');
    }

    this.scope = result;

    return this;
};


ComponentScope.prototype.findByTag = function (tag) {

    var result = [];

    jQuery.each(this.scope, function (index, component) {
        jQuery.each(component.$children, function (index, child) {
            if (child.tag === tag) {
                result.push(child);
            }
        });
    });

    if (result.length === 0) {
        console.warn(this.scope);
        throw new Error('Cannot find any component by tag "' + tag + '" in component scope');
    }

    this.scope = result;

    return this;
};

ComponentScope.prototype.findByTagPrefix = function (tagPrefix) {

    var result = [];

    jQuery.each(this.scope, function (index, component) {
        jQuery.each(component.$children, function (index, child) {
            if (child.tag.lastIndexOf(tagPrefix, 0) === 0) {
                result.push(child);
            }
        });
    });

    if (result.length === 0) {
        console.warn(this.scope);
        throw new Error('Cannot find any component by tag prefix "' + tagPrefix + '" in component scope');
    }

    this.scope = result;

    return this;
};


ComponentScope.prototype.findByTagSuffix = function (tagSuffix) {

    var result = [];

    jQuery.each(this.scope, function (index, component) {
        jQuery.each(component.$children, function (index, child) {
            if (child.tag.slice(-tagSuffix.length) === tagSuffix) {
                result.push(child);
            }
        });
    });

    if (result.length === 0) {
        console.warn(this.scope);
        throw new Error('Cannot find any component by tag suffix "' + tagSuffix + '" in component scope');
    }

    this.scope = result;

    return this;
};

ComponentScope.prototype.filterByRef = function (ref) {

    var result = [];

    jQuery.each(this.scope, function (index, component) {
        if (component.ref === ref) {
            result.push(component);
        }
    });

    if (result.length === 0) {
        console.warn(this.scope);
        throw new Error('Cannot filter components by ref "' + ref + '" in component scope');
    }

    this.scope = result;

    return this;
};

ComponentScope.prototype.filterByTag = function (tag) {

    var result = [];

    jQuery.each(this.scope, function (index, component) {
        if (component.tag === tag) {
            result.push(component);
        }
    });

    if (result.length === 0) {
        console.warn(this.scope);
        throw new Error('Cannot filter components by tag "' + tag + '" in component scope');
    }

    this.scope = result;

    return this;
};

ComponentScope.prototype.filterByTagPrefix = function (tagPrefix) {

    var result = [];

    jQuery.each(this.scope, function (index, component) {
        if (component.tag.lastIndexOf(tagPrefix, 0) === 0) {
            result.push(component);
        }
    });

    if (result.length === 0) {
        console.warn(this.scope);
        throw new Error('Cannot filter components by tag prefix "' + tagPrefix + '" in component scope');
    }

    this.scope = result;

    return this;
};


ComponentScope.prototype.filterByTagSuffix = function (tagSuffix) {

    var result = [];

    jQuery.each(this.scope, function (index, component) {
        if (component.tag.slice(-tagSuffix.length) === tagSuffix) {
            result.push(component);
        }
    });

    if (result.length === 0) {
        console.warn(this.scope);
        throw new Error('Cannot filter components by tag suffix "' + tagSuffix + '" in component scope');
    }

    this.scope = result;

    return this;
};

ComponentScope.prototype.each = function (callback) {
    jQuery.each(this.scope, callback);
};

ComponentScope.prototype.first = function () {
    return this.scope[0];
};

ComponentScope.prototype.findByUid = function (uid) {
    var result = [];

    jQuery.each(this.scope, function (index, component) {
        jQuery.each(component.$children, function (index, child) {
            if (child._uid === parseInt(uid)) {
                result.push(child);
            }
        });
    });

    if (result.length === 0) {
        console.warn(this.scope);
        throw new Error('Cannot find component by uid ' + uid + ' in component scope');
    }

    this.scope = result;

    return this;
};

ComponentScope.prototype.filterByUid = function (uid) {
    var result = [];

    jQuery.each(this.scope, function (index, component) {
        if (component._uid === uid) {
            result.push(component);
        }
    });

    if (result.length === 0) {
        console.warn(this.scope);
        throw new Error('Cannot filter component by uid ' + uid + ' in component scope');
    }

    this.scope = result;

    return this;
};

ComponentScope.prototype.deepFindByUid = function(uid){

    try{
        var component = this.findByUid(uid);
    }
    catch(error) {
        return this.findAll().deepFindByUid(uid);
    }

    return component;
};