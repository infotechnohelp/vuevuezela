var Component = function(rootComponent, uid){
    this.self = new Root(rootComponent).deepFindByUid(uid).first();

    this.jQuery = jQuery(this.self.$el);
};