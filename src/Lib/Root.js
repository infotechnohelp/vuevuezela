var Root = function (component) {
    ComponentScope.call(this, [component.$root]);
};

Root.prototype = Object.create(ComponentScope.prototype);

Root.prototype.constructor = Root;