var tabGroupComponent = Vue.component('tab-group', {
    extends: _groupComponent,
    mixins: [_basicDivSlotTemplateMixin, addTagClassMixin]
});