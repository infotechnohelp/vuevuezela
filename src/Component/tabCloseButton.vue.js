var tabCloseButtonComponent = Vue.component('tab-close-button', {
    extends: _tabCloseComponent,
    mixins: [addTagClassMixin],
    template: '<button @click="clicked()"><slot></slot></button>'
});