var tabComponent = Vue.component('tab-component', {
    extends: _switchTargetComponent,
    mixins: [addTagClassMixin],
    data: function () {
        return {
            // groupedComponentMixin
            groupMemberTagPrefix: 'tab'
        };
    }
});