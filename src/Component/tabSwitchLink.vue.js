var tabSwitchLinkComponent = Vue.component('tab-switch-link', {
    extends: _tabSwitchComponent,
    mixins: [addTagClassMixin],
    template: '<span @click="clicked()"><slot></slot></span>'
});