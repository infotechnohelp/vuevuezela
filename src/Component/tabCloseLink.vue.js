var tabCloseLinkComponent = Vue.component('tab-close-link', {
    extends: _tabCloseComponent,
    mixins: [addTagClassMixin],
    template: '<span @click="clicked()"><slot></slot></span>'
});