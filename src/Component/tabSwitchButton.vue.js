var tabSwitchButtonComponent = Vue.component('tab-switch-button', {
    extends: _tabSwitchComponent,
    mixins: [addTagClassMixin],
    template: '<button @click="clicked()"><slot></slot></button>'
});