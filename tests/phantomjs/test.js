var page = require('webpage').create();
var fs = require('fs');

var args = require('system').args;
var localhostPath = args[1];
var testName = args[2];
var useBuild = (typeof args[3] !== 'undefined') ? '?useBuild' : '';

page.open(localhostPath + '/tests/TestCase/' + testName + '/index.php ' + useBuild);

page.onConsoleMessage = function(msg) {
    console.log('CONSOLE: ' + msg);
};

do {
    phantom.page.sendEvent('mousemove');
} while (
    page.evaluate(function () {
        return typeof document.tester === 'undefined' || !document.tester.testsFinished;
    })
    );

fs.write('tests/phantomjs/results/' + testName + '.html', page.content, 'w');

phantom.exit();