var firstComponent = vueRoot.$children[0];

var constructorParam = [firstComponent.$root];

var classTitle = ComponentScope;

// Test findByRef()
tester.assertLength(new classTitle(constructorParam).findByRef('group1').scope, 1);
tester.assertEquals(new classTitle(constructorParam).findByRef('group1').first()._uid, 1);

try {
    new classTitle(constructorParam).findByRef('justGroup')
} catch (error) {
    tester.assertEquals(error.message, 'Cannot find any component by ref "justGroup" in component scope');
}

// Test findByTag()
tester.assertLength(new classTitle(constructorParam).findByTag('any-component').scope, 3);

try {
    new classTitle(constructorParam).findByTag('any-componen')
} catch (error) {
    tester.assertEquals(error.message, 'Cannot find any component by tag "any-componen" in component scope');
}

// Test findByTagPrefix()
tester.assertLength(new classTitle(constructorParam).findByTagPrefix('any').scope, 3);

try {
    new classTitle(constructorParam).findByTagPrefix('am')
} catch (error) {
    tester.assertEquals(error.message, 'Cannot find any component by tag prefix "am" in component scope');
}

// Test findByTagSuffix()
tester.assertLength(new classTitle(constructorParam).findByTagSuffix('component').scope, 3);

try {
    new classTitle(constructorParam).findByTagSuffix('nothing')
} catch (error) {
    tester.assertEquals(error.message, 'Cannot find any component by tag suffix "nothing" in component scope');
}

// Test filterByRef()
tester.assertEquals(
    new classTitle(constructorParam).findAll().filterByRef('group1').first()._uid, 1);

try {
    new classTitle(constructorParam).findAll().filterByRef('justGroup')
} catch (error) {
    tester.assertEquals(error.message, 'Cannot filter components by ref "justGroup" in component scope');
}

// Test filterByTag() + Depth search
tester.assertLength(
    new classTitle(constructorParam).findByRef('group1').findAll().filterByTag('any-component').scope, 2);

try {
    new classTitle(constructorParam).findByRef('group1').findAll().filterByTag('any-componen')
} catch (error) {
    tester.assertEquals(error.message, 'Cannot filter components by tag "any-componen" in component scope');
}

// Test filterByTagPrefix() + Depth search
tester.assertLength(
    new classTitle(constructorParam).findByRef('group1').findAll().filterByTagPrefix('any').scope, 2);

try {
    new classTitle(constructorParam).findByRef('group1').findAll().filterByTagPrefix('am')
} catch (error) {
    tester.assertEquals(error.message, 'Cannot filter components by tag prefix "am" in component scope');
}

// Test filterByTagSuffix() + Depth search
tester.assertLength(
    new classTitle(constructorParam).findByRef('group1').findAll().filterByTagSuffix('here').scope, 1);

try {
    new classTitle(constructorParam).findByRef('group1').findAll().filterByTagSuffix('here')
} catch (error) {
    tester.assertEquals(error.message, 'Cannot filter components by tag suffix "here" in component scope');
}

// Test each()
var uids = [1, 6, 7, 8];
var i = 0;

new classTitle(constructorParam).findAll().each(function (index, component) {
    tester.assertEquals(component._uid, uids[i++]);
});

// Test first()
tester.assertEquals(new classTitle(constructorParam).findAll().first()._uid, 1);

// Test findByUid()
tester.assertEquals(new classTitle(constructorParam).findByUid(1).first()._uid, 1);
tester.assertEquals(new classTitle(constructorParam).findByUid(6).first()._uid, 6);

// Test filterByUid()
tester.assertEquals(new classTitle(constructorParam).filterByUid(0).first()._uid, 0);
tester.assertEquals(new classTitle(constructorParam).findByTag('any-component').filterByUid(6).first()._uid, 6);

// Test deepFindByUid()
tester.assertEquals(new classTitle(constructorParam).deepFindByUid(2).first()._uid, 2);
tester.assertEquals(new classTitle(constructorParam).deepFindByUid(3).first()._uid, 3);

tester.finish();