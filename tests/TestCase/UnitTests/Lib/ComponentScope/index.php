<?php
function root(){$a = null;$b = 0;
while ($c = true) {if (file_exists(str_repeat('../', $b) . 'src')) {$a = str_repeat('../', $b);break;}
if ($b > 10) {break;}$b++;}return $a;}

require_once(root() . 'builds/Lib/BuildHelper.php');
require_once(root() . 'tests/Lib/TestHelper.php');
require_once(root() . 'tests/Lib/VuevuezelaTestHelper.php');
?>

<script src="<?= root(); ?>node_modules/jquery/dist/jquery.min.js"></script>
<script src="<?= root(); ?>node_modules/vue/dist/vue.min.js"></script>

<?php

if (array_key_exists('useBuild', $_GET)) {
    echo '<script src="' . root() . 'builds/tmp/' . BuildHelper::getLatestTmpBuild() . '/vuevuezela.min.js"></script>';
} else {
    echo VuevuezelaTestHelper::prepareRequired(__DIR__, root());
}
?>

<link rel="stylesheet" href="<?= VuevuezelaTestHelper::prepareAssetPath(__DIR__, 'styles.css'); ?>"/>

<?php require_once(VuevuezelaTestHelper::prepareAssetPath(__DIR__, 'vuevuezela.html')) ?>

<script src="<?= VuevuezelaTestHelper::prepareAssetPath(__DIR__, 'vuevuezela.js'); ?>"></script>

<script src="<?= root(); ?>tests/Lib/Queue.js"></script>
<script src="<?= root(); ?>tests/Lib/Tester.js"></script>

<script src="tests.js"></script>