// Test prepareTemplate()
tester.assertEquals(vuevuezela.prepareTemplate('<div><slot></slot></div>', ["@click='clicked()'"]),
    "<div @click='clicked()'><slot></slot></div>");
tester.assertEquals(vuevuezela.prepareTemplate('<div><slot></slot></div>', ['@click="clicked()"']),
    '<div @click="clicked()"><slot></slot></div>');

tester.assertEquals(vuevuezela.prepareTemplate('<div><slot></slot></div>', [
        '@click3="clicked3()"',
        '@click2="clicked2()"',
        "@click='clicked()'"
    ]),
    "<div @click3=\"clicked3()\" @click2=\"clicked2()\" @click='clicked()'><slot></slot></div>");


var functionality = {
    templateInjection: "@click='clicked()'"
};

var instruction = {
    functionalities: [functionality]
};

// Test getTemplateInjections, not extending instruction
tester.assertSame(vuevuezela.getTemplateInjections(instruction), ["@click='clicked()'"]);
tester.assertNotSame(vuevuezela.getTemplateInjections(instruction), ['@click="clicked()"']);


var functionality2 = {
    templateInjection: '@click2="clicked2()"'
};

var instruction2 = {
    functionalities: [functionality2],
    extending: {
        instruction: instruction
    }
};


// Test getTemplateInjections, extending instruction, 2 levels
tester.assertSame(vuevuezela.getTemplateInjections(instruction2), [
    '@click2="clicked2()"',
    "@click='clicked()'"
]);

var functionality3 = {
    templateInjection: '@click3="clicked3()"'
};

var instruction3 = {
    functionalities: [functionality3],
    extending: {
        instruction: instruction2
    }
};

// Test getTemplateInjections, extending instruction, 3 levels
tester.assertSame(vuevuezela.getTemplateInjections(instruction3), [
    '@click3="clicked3()"',
    '@click2="clicked2()"',
    "@click='clicked()'"
]);

tester.finish();