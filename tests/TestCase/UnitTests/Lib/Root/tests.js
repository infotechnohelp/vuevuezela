var firstComponent = vueRoot.$children[0];

var classTitle = Root;

// Test filterByTag() + Depth search
tester.assertSame(
    new classTitle(firstComponent).findByRef('group1').findAll().filterByTag('any-component').scope,
    new ComponentScope([firstComponent.$root]).findByRef('group1').findAll().filterByTag('any-component').scope);

tester.finish();