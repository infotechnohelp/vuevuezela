var anyComponent = Vue.component('any-component', {
    template: "<div><slot></slot></div>",
    mixins: [componentMixin]
});

var anotherComponent = Vue.component('another-component-here', {
    template: "<div><slot></slot></div>",
    mixins: [componentMixin]
});


var vueRoot = new Vue({
    el: '#vue-root'
});