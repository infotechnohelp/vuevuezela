jQuery(function () {

    var $elem = jQuery('#test1-1');

    //tester.displaySuccess = true;

    // Test separate tab-switch-button with :state_changed_config
    queue.next(function () {
        // Assert that text is rendered and all tab-components are hidden
        tester.assertEquals('Separate 1 tab', $elem.text().trim());

        jQuery('.tab-component').each(function (index, element) {
            tester.assertEquals('none', jQuery(element).css('display'));
        });

        $elem.click();
    }).next(function () {
        // Assert that button's text and style have changed, only corresponding tab-component displayed
        tester.assertEquals('Close', $elem.text());

        tester.assertEquals('rgb(0, 128, 0)', $elem.css('background-color')); // green
        tester.assertEquals('pointer', $elem.css('cursor'));

        tester.assertTrue($elem.hasClass('bold'));

        jQuery('.tab-component').each(function (index, element) {
            if (jQuery(element).attr('id') === 'test8-1') {
                tester.assertNotEquals('none', jQuery(element).css('display'));
            } else {
                tester.assertEquals('none', jQuery(element).css('display'));
            }
        });

        $elem.click();
    }).next(function () {
        // Assert that button's text and style are refreshed, all tab-components are hidden
        tester.assertEquals('Separate 1 tab', $elem.text());

        tester.assertNotEquals('rgb(0, 128, 0)', $elem.css('background-color')); // green
        tester.assertNotEquals('pointer', $elem.css('cursor'));
        tester.assertNotEquals('700', $elem.css('font-weight')); // bold

        tester.assertFalse($elem.hasClass('bold'));

        jQuery('.tab-component').each(function (index, element) {
            tester.assertEquals('none', jQuery(element).css('display'));
        });

        $elem.click();
    }).next(function () {
        jQuery('#test8-1-1').click();
    }).next(function () {
        // Test that tab-close-button works correctly: button's text and style refreshed, all tab-components are hidden
        tester.assertEquals('Separate 1 tab', $elem.text());

        tester.assertNotEquals('rgb(0, 128, 0)', $elem.css('background-color')); // green
        tester.assertNotEquals('pointer', $elem.css('cursor'));
        tester.assertNotEquals('700', $elem.css('font-weight')); // bold

        tester.assertFalse($elem.hasClass('bold'));

        jQuery('.tab-component').each(function (index, element) {
            tester.assertEquals('none', jQuery(element).css('display'));
        });
    });

    // Test separate tab-switch-link with :refresh_on_click=false
    queue.next(function () {
        $elem = jQuery('#test1-2');

        $elem.click();
    }).next(function () {
        // Assert that only corresponding tab-component is displayed, link's color has become purple
        jQuery('.tab-component').each(function (index, element) {
            if (jQuery(element).attr('id') === 'test8-2') {
                tester.assertNotEquals('none', jQuery(element).css('display'));
            } else {
                tester.assertEquals('none', jQuery(element).css('display'));
            }
        });

        tester.assertEquals('rgb(128, 0, 128)', $elem.css('color')); // purple

        $elem.click();
    }).next(function () {
        // Assert that corresponding tab-component is still displayed
        jQuery('.tab-component').each(function (index, element) {
            if (jQuery(element).attr('id') === 'test8-2') {
                tester.assertNotEquals('none', jQuery(element).css('display'));
            } else {
                tester.assertEquals('none', jQuery(element).css('display'));
            }
        });

        jQuery('#test8-2-1').click();
    }).next(function () {
        // Assert that all tab-components are closed, link's color is not purple anymore
        jQuery('.tab-component').each(function (index, element) {
            tester.assertEquals('none', jQuery(element).css('display'));
        });

        tester.assertNotEquals('rgb(128, 0, 128)', $elem.css('color')); // purple
    });

    // Test separate tab-switch-button & tab-switch-link twins with :state_changed_config with 2 corresponding tabs
    queue.next(function () {
        $elem = jQuery('#test1-3');

        $elem.click();
    }).next(function () {
        // Assert that both twins are activated, their style has changed and both corresponding tabs are displayed
        tester.assertTrue($elem.hasClass('active'));
        tester.assertTrue($elem.hasClass('underlined'));
        tester.assertTrue($elem.hasClass('bold'));

        var elemTwin = jQuery('#test1-4');

        tester.assertTrue(elemTwin.hasClass('active'));
        tester.assertEquals('rgb(0, 128, 0)', elemTwin.css('color')); // green

        jQuery('.tab-component').each(function (index, element) {
            if (jQuery(element).attr('id') === 'test8-3' || jQuery(element).attr('id') === 'test8-4') {
                tester.assertNotEquals('none', jQuery(element).css('display'));
            } else {
                tester.assertEquals('none', jQuery(element).css('display'));
            }
        });

        elemTwin.click();
    }).next(function () {
        // Assert that both twins are refreshed, all tab-components are hidden
        tester.assertFalse($elem.hasClass('active'));
        tester.assertFalse($elem.hasClass('underlined'));
        tester.assertFalse($elem.hasClass('bold'));

        var elemTwin = jQuery('#test1-4');

        tester.assertFalse(elemTwin.hasClass('active'));
        tester.assertNotEquals('rgb(0, 128, 0)', elemTwin.css('color')); // green

        jQuery('.tab-component').each(function (index, element) {
            tester.assertEquals('none', jQuery(element).css('display'));
        });
    });

    // Test a grouped tab-switch with a twin and :refresh_on_click=false, group with :multiple_active_members
    queue.next(function () {
        $elem = jQuery('#test2-1');

        $elem.click();
    }).next(function () {
        // Assert that corresponding tab is opened
        jQuery('.tab-component').each(function (index, element) {
            if (jQuery(element).attr('id') === 'test6-1') {
                tester.assertNotEquals('none', jQuery(element).css('display'));
            } else {
                tester.assertEquals('none', jQuery(element).css('display'));
            }
        });

        $elem.click();
    }).next(function () {
        // Assert that tab is still displayed
        jQuery('.tab-component').each(function (index, element) {
            if (jQuery(element).attr('id') === 'test6-1') {
                tester.assertNotEquals('none', jQuery(element).css('display'));
            } else {
                tester.assertEquals('none', jQuery(element).css('display'));
            }
        });

        var tabSwitchTwin = jQuery('#test4-1');

        tabSwitchTwin.click();
    }).next(function () {
        // Assert that tab is still displayed
        jQuery('.tab-component').each(function (index, element) {
            if (jQuery(element).attr('id') === 'test6-1') {
                tester.assertNotEquals('none', jQuery(element).css('display'));
            } else {
                tester.assertEquals('none', jQuery(element).css('display'));
            }
        });

        var tabCloseButton = jQuery('#test6-1-1');

        tabCloseButton.click();
    }).next(function () {
        // Assert that all tabs are hidden
        jQuery('.tab-component').each(function (index, element) {
            tester.assertEquals('none', jQuery(element).css('display'));
        });

        $elem.click();
    }).next(function () {
        // Assert that both twins are active
        tester.assertTrue($elem.hasClass('active'));

        var tabSwitchTwin = jQuery('#test4-1');
        tester.assertTrue(tabSwitchTwin.hasClass('active'));

        var tabSwitchSibling = jQuery('#test2-2');

        tabSwitchSibling.click();
    }).next(function () {
        // Assert that all siblings are active
        tester.assertTrue($elem.hasClass('active'));

        var tabSwitchTwin = jQuery('#test4-1');
        tester.assertTrue(tabSwitchTwin.hasClass('active'));

        var tabSwitchSibling1 = jQuery('#test2-2');
        tester.assertTrue(tabSwitchSibling1.hasClass('active'));

        var tabSwitchSibling2 = jQuery('#test4-2');
        tester.assertTrue(tabSwitchSibling2.hasClass('active'));

        tabSwitchSibling1.click();
    }).next(function () {
        // Assert that tabSwitch and its twin are still active
        tester.assertTrue($elem.hasClass('active'));

        var tabSwitchTwin = jQuery('#test4-1');
        tester.assertTrue(tabSwitchTwin.hasClass('active'));

        var tabCloseButton = jQuery('#test6-1-1');

        tabCloseButton.click();
    }).next(function () {
        // Assert that all tab switch twins are not active
        tester.assertFalse($elem.hasClass('active'));

        var tabSwitchTwin = jQuery('#test4-1');
        tester.assertFalse(tabSwitchTwin.hasClass('active'));

        var tabSwitchSibling1 = jQuery('#test2-2');
        tester.assertFalse(tabSwitchSibling1.hasClass('active'));

        var tabSwitchSibling2 = jQuery('#test4-2');
        tester.assertFalse(tabSwitchSibling2.hasClass('active'));
    });

    // Test a grouped tab-switch with a twin
    queue.next(function () {
        $elem = jQuery('#test3-1');

        $elem.click();
    }).next(function () {
        // Assert only tabSwitch and its twin are active, siblings are not
        tester.assertTrue($elem.hasClass('active'));

        var tabSwitchTwin = jQuery('#test5-1');
        tester.assertTrue(tabSwitchTwin.hasClass('active'));

        var tabSwitchSibling1 = jQuery('#test3-2');
        tester.assertFalse(tabSwitchSibling1.hasClass('active'));

        var tabSwitchSibling2 = jQuery('#test5-2');
        tester.assertFalse(tabSwitchSibling2.hasClass('active'));

        tabSwitchSibling1.click();
    }).next(function () {
        // Assert that tab switch siblings are active, tab switch and its twin is not
        tester.assertFalse($elem.hasClass('active'));

        var tabSwitchTwin = jQuery('#test5-1');
        tester.assertFalse(tabSwitchTwin.hasClass('active'));

        var tabSwitchSibling1 = jQuery('#test3-2');
        tester.assertTrue(tabSwitchSibling1.hasClass('active'));

        var tabSwitchSibling2 = jQuery('#test5-2');
        tester.assertTrue(tabSwitchSibling2.hasClass('active'));

        tabSwitchSibling2.click();
        tester.finish();
    });
});