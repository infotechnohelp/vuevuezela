var Queue = function () {
    this.steps = [jQuery.Deferred()];
    this.delayMiliseconds = 10;
};

Queue.prototype.next = function (callback) {

    if (this.steps.length === 1) {
        this.step(this.steps[0], callback);
    }
    else {
        var $this = this;

        var stepsLength = this.steps.length;

        this.steps[(stepsLength - 2)].done(function () {
            setTimeout(function () {
                $this.step($this.steps[(stepsLength - 1)], callback);
            }, $this.delayMiliseconds);
        });
    }

    this.steps.push(jQuery.Deferred());

    return this;
};

Queue.prototype.delay = function (miliseconds) {
    this.delayMiliseconds = miliseconds;

    return this;
};

Queue.prototype.step = function (deferred, stepCallback) {
    stepCallback();
    deferred.resolve();
};

var queue = new Queue();