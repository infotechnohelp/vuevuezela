<?php

/**
 * Class VuevuezelaTestHelper
 */
class VuevuezelaTestHelper extends TestHelper
{
    /**
     * @param $testPath
     * @param $rootPath
     *
     * @return string
     */
    static function prepareRequired($testPath, $rootPath)
    {
        $result = '';

        if ( ! defined('DS')) {
            define('DS', DIRECTORY_SEPARATOR);
        }

        $json = file_get_contents(self::prepareAssetPath($testPath, 'required.json'));

        foreach (json_decode($json, true) as $index => $required) {
            if (in_array(explode('/', $required)[0], ['BaseComponent', 'Component'])) {
                $result .= "<script src='$rootPath" . "src/$required.vue.js'></script>\n";
            } else {
                $result .= "<script src='$rootPath" . "src/$required.js'></script>\n";
            }

        }

        return $result;
    }

    /**
     * @param $testPath
     * @param $assetTitle
     *
     * @return string
     */
    static function prepareAssetPath($testPath, $assetTitle)
    {
        $relativePathPrefix = null;
        $depth              = 0;

        while ($c = true) {
            if (file_exists($testPath . DIRECTORY_SEPARATOR . $assetTitle)) {
                break;
            }

            if (file_exists($testPath . DIRECTORY_SEPARATOR . 'tests')) {
                return str_repeat('../', $depth) . 'tests/EmptyAssets/' . $assetTitle;
            }

            $testPath = dirname($testPath);
            $depth++;
        }

        return str_repeat('../', $depth) . $assetTitle;
    }
}