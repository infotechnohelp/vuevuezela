var Tester = function () {
    this.successMessage = '.';
    this.failureMessage = 'Failed assertion';
    this.displaySuccess = false;
    this.throwError = false;

    this.assertionAmount = 0;
    this.failureAmount = 0;

    this.failedAssertions = [];

    this.testsFinished = false;
};

Tester.prototype.finish = function () {
    var result = this.assertionAmount + ' assertions, ' + this.failureAmount + ' failures ' + JSON.stringify(this.failedAssertions);

    console.log(result);

    jQuery('body').append('<div id="tester-output">' + result + '</div>');

    this.testsFinished = true;
};

Tester.prototype.assert = function (assertion, displaySuccess) {
    this.assertionAmount++;

    if (typeof displaySuccess === 'undefined') {
        displaySuccess = this.displaySuccess;
    }

    if (assertion) {
        if (displaySuccess) {
            console.warn(this.assertionAmount + ':' + this.successMessage);
        }
    } else {
        this.failureAmount++;
        this.failedAssertions.push(this.assertionAmount);

        if (this.throwError) {
            throw new Error(this.failureMessage);
        } else {
            console.warn(this.failureMessage);
        }
    }
};

Tester.prototype.isEquivalent = function (a, b) {

    var aProps = Object.getOwnPropertyNames(a);
    var bProps = Object.getOwnPropertyNames(b);

    if (aProps.length !== bProps.length) {
        return false;
    }

    for (var i = 0; i < aProps.length; i++) {
        var propName = aProps[i];

        if (a[propName] !== b[propName]) {
            return false;
        }
    }

    return true;
};

Tester.prototype.assertSame = function (value, expected, displaySuccess) {
    this.assert(this.isEquivalent(value, expected), displaySuccess);
};

Tester.prototype.assertNotSame = function (value, expected, displaySuccess) {
    this.assert(!this.isEquivalent(value, expected), displaySuccess);
};

Tester.prototype.assertEquals = function (value, expected, displaySuccess) {
    this.assert(value === expected, displaySuccess);
};

Tester.prototype.assertNotEquals = function (value, expected, displaySuccess) {
    this.assert(value !== expected, displaySuccess);
};

Tester.prototype.assertTrue = function (value, displaySuccess) {
    this.assert(value === true, displaySuccess);
};

Tester.prototype.assertFalse = function (value, displaySuccess) {
    this.assert(value === false, displaySuccess);
};

Tester.prototype.assertLength = function (value, expected, displaySuccess) {
    this.assert(value.length === expected, displaySuccess);
};

Tester.prototype.assertUndefined = function (value, expected, displaySuccess) {
    this.assert(value === undefined, displaySuccess);
};

Tester.prototype.assertNotUndefined = function (value, expected, displaySuccess) {
    this.assert(value !== undefined, displaySuccess);
};


document.tester = new Tester();

var tester = document.tester;