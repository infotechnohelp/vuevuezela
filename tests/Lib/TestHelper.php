<?php

/**
 * Class TestHelper
 */
class TestHelper
{
    /**
     * @return mixed
     */
    static function getLocalhostPath()
    {
        $localhostPath = str_replace('C:\\xampp\\htdocs\\', 'http://localhost/', dirname(dirname(__DIR__)));

        return str_replace('\\', '/', $localhostPath);
    }

    /**
     * @param string $dirPath
     *
     * @return bool
     *
     */
    static function isTestCase(string $dirPath): bool
    {
        if (file_exists($dirPath . DS . 'index.php')) {
            return true;
        }

        return false;
    }

    /**
     * @param string $path
     * @param array  $testCaseList
     */
    static function findTestCase(string $path, array &$testCaseList)
    {
        if ( self::isTestCase($path)) {
            $path           = str_replace('\\', '/', $path);
            $testCaseList[] = substr($path, strpos($path, 'TestCase') + strlen('TestCase') + 1);
        } else {
            $dirs = glob($path . DS . '*', GLOB_ONLYDIR);

            foreach ($dirs as $dir) {
                self::findTestCase($dir, $testCaseList);
            }
        }
    }

    /**
     * @return array
     */
    static function getTestList()
    {
        $result = [];

        if ( ! defined('DS')) {
            define('DS', DIRECTORY_SEPARATOR);
        }

        $testCasePath = dirname(__DIR__) . DS . 'TestCase';

        self::findTestCase($testCasePath, $result);

        return $result;
    }

    /**
     * @param string $testTitle
     *
     * @return null
     */
    static function getTesterOutput(string $testTitle)
    {
        $phantomjsResultPath = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'phantomjs' . DIRECTORY_SEPARATOR .
                               'results' . DIRECTORY_SEPARATOR . $testTitle . '.html';

        $phantomjsResult = file_get_contents($phantomjsResultPath);

        if (preg_match('/\<div id="tester-output"\>(.*?)\<\/div\>/', $phantomjsResult, $match) === 1) {
            return $match[1];
        }

        return null;
    }
}