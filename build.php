<?php

require('builds/Lib/BuildHelper.php');

$sourceFilePaths = BuildHelper::sourceFilePaths(true);

exec("uglifyjs $sourceFilePaths 2>&1", $output, $return_var);

$buildDir = BuildHelper::createBuildDir();

BuildHelper::writeToBuildFile($buildDir, $output[0]);

$compressionConfig = "-c";

$mangleConfig = "-m keep_fnames";

exec("uglifyjs $sourceFilePaths $compressionConfig $mangleConfig 2>&1", $minOutput, $return_var);

BuildHelper::writeToBuildMinFile($buildDir, $minOutput[0]);