<?php

require('tests/Lib/TestHelper.php');

// XAMPP is used
$localhostPath = TestHelper::getLocalhostPath();

$printConsoleOutput = (count($argv) > 1 && $argv[1]) ? true : false;

$useBuild = (count($argv) > 2 && $argv[2]) ? true : '';

foreach (TestHelper::getTestList() as $testTitle) {
    exec("phantomjs tests/phantomjs/test.js $localhostPath $testTitle $useBuild", $output);

    if($printConsoleOutput){
        print_r($output);
    }
}

foreach (TestHelper::getTestList() as $testTitle) {
    echo $testTitle . ': ' . TestHelper::getTesterOutput($testTitle) . "\n";
}