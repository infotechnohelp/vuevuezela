## FOR USERS

### Dependencies

#### CDN
jQuery 2+

`https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js`

Vue 2+

`https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.17/vue.min.js`

### Installation

#### NPM
`npm install vuevuezela` || `npm install vuevuezela@0.0.1`

#### Yarn
`yarn add vuevuezela` || `yarn add  vuevuezela@0.0.1`

#### Bower
`bower install vuevuezela` || `bower install vuevuezela#0.0.1`

#### CDN
`https://cdn.vuevuezela.com/0.0.1/vuevuezela.js` || `https://cdn.vuevuezela.com/0.0.1/vuevuezela.min.js`


## FOR DEVELOPERS

### Dependencies

#### XAMPP 
(localhost, global OS variable: php)

Project path: `xampp/htdocs`

#### PhantomJS 
(global OS variable: phantomjs)

#### UglifyJS 2
(global OS variable: uglifyjs)

`npm install uglify-js -g`

##### Command line:

`npm install`

### Tests

#### Terminal command (from root)
`php test.php`

##### useBuild 

`php test.php [any value]`

Uses latest build from `builds/tmp/` while testing

### Build

#### Terminal command (from root)

`php build.php`

Output → `builds/tmp/[timestamp]/vueveuzela.js`

Output → `builds/tmp/[timestamp]/vueveuzela.min.js`

### Conventions

#### Naming

* TemplateMixin

TemplateMixin title: `[AnyTitle]`

TemplateMixin file path: `src/TemplateMixin/_[TemplateMixin title].js`

TemplateMixin variable: `var _[TemplateMixin title]TemplateMixin`

* Mixin

Mixin title: `[AnyTitle]`

Mixin file path: `src/Mixin/[Mixin title].js`

Mixin variable: `var [Mixin title]Mixin`

* BaseComponent

BaseComponent title: `[AnyTitle]`

BaseComponent file path: `src/BaseComponent/_[BaseComponent title].vue.js`

BaseComponent variable: `var _[BaseComponent title]Component`


* Component

Component title: `[AnyTitle]`

Component file path: `src/Component/[Component title].vue.js`

Component variable: `var [Component title]Component`