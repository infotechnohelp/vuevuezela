<?php

/**
 * Class BuildHelper
 */
class BuildHelper
{
    /**
     * @param bool $returnString
     *
     * @return array|string
     */
    static function sourceFilePaths(bool $returnString = false)
    {
        $result      = [];
        $dirContents = [];

        $sourceDirs = glob(dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR .
                           'src' . DIRECTORY_SEPARATOR . '*',
            GLOB_ONLYDIR);

        foreach ($sourceDirs as $sourceDir) {
            $sourceFiles = glob($sourceDir . DIRECTORY_SEPARATOR . '*.{js}', GLOB_BRACE);

            $dirContents[substr($sourceDir, strrpos($sourceDir, DIRECTORY_SEPARATOR) + 1)] = $sourceFiles;
        }

        $dirOrder = ['Lib', 'Mixin', 'TemplateMixin', 'BaseComponent', 'Component'];

        foreach ($dirOrder as $dirTitle) {
            $result = array_merge($result, $dirContents[$dirTitle]);
        }

        if ($returnString) {
            return implode(' ', $result);
        }

        return $result;
    }

    /**
     * @return string
     */
    static function createBuildDir()
    {
        $time = time();

        $buildDirPath = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . $time;

        if ( ! file_exists($buildDirPath)) {
            mkdir($buildDirPath);
        }

        return $buildDirPath;
    }

    /**
     * @param string $buildDirPath
     * @param string $contents
     */
    static function writeToBuildFile(string $buildDirPath, string $contents)
    {
        $buildFilePath = $buildDirPath . DIRECTORY_SEPARATOR . 'vuevuezela.js';
        $buildFile     = fopen($buildFilePath, "w");
        fwrite($buildFile, $contents);
        fclose($buildFile);
    }

    /**
     * @param string $buildDirPath
     * @param string $contents
     */
    static function writeToBuildMinFile(string $buildDirPath, string $contents)
    {
        $buildFilePath = $buildDirPath . DIRECTORY_SEPARATOR . 'vuevuezela.min.js';
        $buildFile     = fopen($buildFilePath, "w");
        fwrite($buildFile, $contents);
        fclose($buildFile);
    }

    static function getLatestTmpBuild()
    {
        $tmpBuildDirPath = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'tmp';
        $buildDirs       = glob($tmpBuildDirPath . DIRECTORY_SEPARATOR . '*', GLOB_ONLYDIR);

        $lastIndex = count($buildDirs) - 1;

        return substr($buildDirs[$lastIndex], strrpos($buildDirs[$lastIndex], DIRECTORY_SEPARATOR) + 1);
    }
}