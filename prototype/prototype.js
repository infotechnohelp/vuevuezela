var component = {
    instance: null,
    init: function (component) {
        if (this.instance === null) {
            this.instance = new Component(component);
        }

        return this.instance;
    },
    template: 'button-slot',
    mixins: [
        'component' /* default ? */,
        'clicked',
        'enabled'
    ],
    visual: {
        switched: function (c, input) {
            jQuery(c).animate(/*...*/).delay(500).animate(/*...*/);
        },
        enabled: function (c, input) {
            jQuery(c).addClass('clicked');
        },
        disabled: function (c, input) {
            jQuery(c).removeClass('clicked')
        }
    },
    action: {
        clicked: {
            clicked: function (c/*instance*/, input) {
                c.action('enabled').switched(input, false/**/);
            }
        },
        enabled: {
            enabled: function (c, input) {
                c.visual().enabled(input);
            },

             disabled: function (c, input) {
                 c.visual().disabled(input);
             },


            switched: function (c, input) {
                c.visual().switched(input);
            },

            // DEFAULT
            switchedDefault: function (c, input) {
                if (c.state('enabled').enabled) {
                    c.state('enabled').enabled = false;
                } else {
                    c.state('enabled').enabled = true;
                }
            }
        }

    }

};
