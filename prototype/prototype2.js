// User action events: click, mouse event, slide, etc.

// Not only user action events should be configured in component instructions, but all others as well

// Event title: 'clicked.clicked', pass uid and tag as params

// this.$root.$emit('clicked.clicked', {uid: 1, tag: 'switch', properties: {});

Vuevuezela.prototype.root = function (id) {

    var Vuevuezela = this;

    return new Vue({
        el: id,
        mounted: function () {
            var VueRoot = this;

            jQuery.each(Vuevuezela.eventListeners, function (index, event) {

                VueRoot.$on(event, function (input) {

                    event[input.tag].action.method(VueRoot, input.uid, input.properties);

                    if (event[input.tag].action.extends !== undefined) {
                        jQuery.each(event[input.tag].action.extends, function (index, tag) {
                            event[tag].action.method(VueRoot, input.uid, input.properties);
                        });
                    }

                    event[input.tag].visual.method(VueRoot, input.uid, input.properties);

                    if (event[input.tag].visual.extends !== undefined) {
                        jQuery.each(event[input.tag].action.extends, function (index, tag) {
                            event[tag].visual.method(VueRoot, input.uid, input.properties);
                        });
                    }
                });
            });
        }
    });
};


var Vuevuezela = function () {
    this.eventListeners = {

        'clicked.clicked': {
            switch: {
                action: {
                    method: function (rootComponent, uid, input) {

                    }
                },
                visual: {
                    method: function (rootComponent, uid, input) {

                    }
                }
            },
            'tab-switch': {
                action: {
                    method: function (rootComponent, uid, input) {

                    },
                    extends: ['switch']
                },
                visual: {
                    method: null,
                    extends: ['switch']
                }
            }
        }

    };
};

var tabSwitchButtonInstructions = {

    // @todo Add default later

    title: 'tab-switch-button',
    template: _buttonSlotClickable,
    extends: [
        {
            instructions: tabSwitchInstructions, // Get extensions (from all parents) → mixins and injections
            ignore: {action: ['clicked.clicked'], visual: ['clicked.clicked']}
            // @todo add ignore.visual from plugin later ???
        }
    ]

};

// instructions should include visual events (enabled)

// Visual events should